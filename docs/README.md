---
home: true
meta:
  - name: "og:title"
    content: "Manual de Sobrevivência das Associações"
  - name: "og:description"
    content: "Um guia para lidar com os problemas burocráticos de uma Associação sem fins lucrativos em Portugal"
  - name: "og:image"
    content: "https://manual-sobrevivencia-associacoes.gitlab.io/ong-survival-manual.png"
  - name: "og:url"
    content: "https://manual-sobrevivencia-associacoes.gitlab.io/"
heroImage: /ong-survival-manual.png
heroText: Manual de sobrevivência das associações
tagline:
  Um guia para lidar com os problemas burocráticos de uma associação sem fins lucrativos em Portugal
actionText: Começar →
actionLink: /guide/
features:
- title: Linguagem simples
  details:
      Legalês não é bem-vindo. As explicações são simples o suficiente para que
      as pessoas das mais diversas áreas consigam entender e transformar o texto
      em prática.
- title: Um projeto colaborativo
  details:
      Boas práticas merecem ser registada. Se souber como fazer algo melhor
      do que está ecrito neste guia, propõe uma modificação. Contributos
      são mais que bem-vindos!
- title: É apenas um manual
  details:
      Este recurso serve como apoio mínimo à sobrevivência de uma associação em Portugal.
      Mas não consegue substituir completamente advogados ou contabilistas.
footer: Creative Commons Attribution Non Commercial 4.0 International
---
