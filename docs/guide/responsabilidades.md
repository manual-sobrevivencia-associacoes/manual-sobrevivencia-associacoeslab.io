# Papeis e responsabilidades

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

A atribuição de responsabilidades burocráticas responsabiliza as
pessoas para as suas tarefas e é neste sentido que a lei
obriga. Descrevemos aqui os vários órgãos, as suas obrigações e
responsabilidades daqueles que os integram.

::: tip AVISO

Todas as posições oficiais dentro da associação estão definidas nos
estatutos bem como o seu âmbito. Portanto é possível que no seu caso
haja mais órgãos e obrigações. Consultar os estatutos em complemento
com esta secção.

:::

Há dois órgãos obrigatórios e um opcional, mas recomendado:
  - **Direção** - detém o poder executivo e é eleita em [Assembleia
    Geral](assembleias-gerais.md)
  - **Conselho Fiscal** - fiscaliza ações da direção e contas em cada
    [assembleia geral ordinária](assembleias-gerais.md)
  - **Mesa da Assembleia** (Opcional) - Coordena e guia a Assembleia Geral


Direção
-------

Responsável por:
  - **poder executivo da associação**
  - **produzir relatórios de atividade** do ano anterior - lista de
    todas as atividades que a associação realizou (realçando o
    impacto, fazendo uma reflexão e conlcuindo se foi ou não um bom
    investimento).

### Presidente

  - Convoca reuniões da direção
  - Representa a associação e a Direção
  - Tem voto de desempate em reuniões da Direção

### Vice-presidente(s)
Subtituto(s) do(a) presidente.

### Tesoureiro(a)
Responsável por matérias financeiras da organização
  - Faz orçamento para ano seguinte
  - Submete e publica resultados fiscais do ano anteriror
  - Gere as despesas e interage com contabilistas, caso necessário.

Ler mais sobre as responsabilidades do(a) Tesoureiro(a) na página
[sobre contabilidade](contabilidade.md).


Conselho Fiscal
---------------

Responsável por fiscalizar internamente as decisões da direção. As
suas decisões são apresentadas em assembleia geral (TODO encontrar
fonte).

### Presidente
  - Convoca reuniões do conselho fiscal
  - Tem voto de desempate em reuniões do conselho fiscal

### Secretários(as)
  - TODO


Assembleia Geral
------------------------

A Assembleia Geral é quando os membros da associação são todos
convidados a reunirem-se. Ler mais sobre este órgão na [sua página
própria](assembleias-gerais.md).

### Membros que nela compareçam
- eleger ou destituir outros órgãos (caso seja esse o processo
  definido nos estatutos) ([código
  civil][cc170])

[cc170]: https://dre.pt/web/guest/legislacao-consolidada/-/lc/123928118/202002020919/73747283/diploma/indice?q=c%C3%B3digo+civil

::: details Assembleia não delibera sobre competências de outro órgão

Para as coisas funcionarem bem, não é da responsabilidade da AG
deliberar sobre matérias da competência de um outro órgão. Mas pode,
por exemplo **chamar à atenção um dado órgão** sobre uma certa decisão
ou até mesmo **destituir membros de órgãos**.

::

### Mesa da assembleia geral
Responsabilidades da mesa da AG estão expliciadas [nesta
página](assembleias-gerais.md#conducao-da-assembleia-geral)
