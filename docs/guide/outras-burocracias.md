# Outras burocracias

## Alterar a morada da sede

::: warning Aviso

:gun: **fuzilamento legal**: Mudar a sede é um processo dispendioso para uma pequena associação.

:::

Passos para mudar a morada:

1. Pedir Certificado de Admissibilidade (no caso de mudança de concelho)
2. Realizar assembleia geral para alterar morada nos estatutos
3. Fazer escritura pública num notário (se IPSS dispensa este passo)
4. Inscrever alteração no RNPC enviando uma carta com documento notarial e vale-postal de 50€


### Tabela de custos

| Custo                  | Passo                                            | Descrição                                                                                      |
|------------------------|--------------------------------------------------|------------------------------------------------------------------------------------------------|
| 75€ (150€ c/ urgência) | Certificado de Admissibilidade                   | É literalmente apenas para aceitarem a mudança de concelho. Nem pedem a morada futura nem nada |
| 150€ - 500€            | Escritura Pública                                | Para publicar mudança aos estatutos online. Preço varia conforme o notário.                    |
| 50€                    | Inscrever alteração no [RNPC](glossario.md#RNPC) | Registar a alteração no RNPC via carta com selo postal                                         |

### Atualizar morada

#### Atualizar no Portal das Finanças

Deverá ser automático. Contudo é sempre bom verificar porque com as finanças não se brinca.


#### Atualizar dados no RCBE

1. Ir ao portal do Registo do Beneficiário Efetivo (tem de ser feito pela pessoa que o submeteu originalmente)
2. Submeter pedido de alteração de dados com documento justificativo (pedido de desconformidade)
3. Esperar uns tempos pelo email de confirmação de que podem alterar os dados
4. Quando receberem o email, podem ir lá alterar os dados (por confirmar).

