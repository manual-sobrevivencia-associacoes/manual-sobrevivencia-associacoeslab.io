# Assembleias Gerais

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

[[toc]]

Ao contrário das empresas, nas associações não é a direção que a
controla mas sim todos os seus associados. E é essa a voz que as
Assembleias Gerais (AGs) representam - a vontade coletiva de todos os
seus membros.

A lei obriga a toda uma série de protocolos sobre o que fazer nas
assembleias gerais para garantir que todos podem ser ouvidos e que as
decisões resultam de opiniões livres e informadas.

No entanto, como muita coisa na lei, o efeito por vezes é o contrário
ao esperado (o tal "blowback") e é o que acontece exatamente nas
AGs. Como têm um conjunto de regras demasiado rígidas acabam por ser
pouco convidativas e o que acontece na prática é que, na maior parte
dos casos, aparecem sempre os mesmos gatos pingados nas assembleias.

::: tip DICA

Para combater isso, no final deste documento temos uma [série de
estratégias](#como-fazer-uma-boa-assembleia-geral)

:::

Antes da Assembleia Geral
-------------------------

Há dois tipos de AGs:

  - **[Ordinárias](#convocar-uma-assembleia-geral-ordinaria)** -
    acontecem pelo menos uma vez por ano e servem para fazer uma série
    de burocracias como **aprovar o balanço** e **relatório de
    atividades**.
	
  - **[Extraordinárias](#convocar-uma-assembleia-geral-extraordinaria)** -
    convocadas em situações excecionais

### Quando é que se convoca uma AG?

Convoca-se uma assembleia geral nestas circunstâncias:
  - **Anualmente (obrigatório)** - para aprovar o balanço e o relatório de atividades ([artigo 173.o, n.o 1, do
> CC](https://dre.pt/web/guest/legislacao-consolidada/-/lc/123928118/201912131603/73747279/diploma/indice))
  - **pelo menos 1/5 dos sócios exigem uma AG**
  - **já tiver passado um ano desde a última** - qualquer sócio a pode
    exigir
  - **direção exigir**
  - outra regra establecida nos estatutos podem outros casos (não se
    aplica a nós)
  - 

 

### Como é que se convoca uma AG?

::: tip NOTA 

Caso não esteja definido nos estatutos, a convocação tem
de ser enviada com 8 dias de antecedência.

:::


Elementos a incluir:
  - **dia, data e local**
  - **tipo da assembleia** - ser é extraordinária ou ordinária
  - **ordem de trabalhos** - têm que ser itens específicos
  - **documentos de apoio** (opcional) - ex: se vão aprovar o
    relatório de contas então este deve ser enviado juntamente com a
    convocatória



### Convocar uma Assembleia Geral Ordinária

> É obrigatória a realização de uma assembleia geral anual para
> aprovação do balanço (relatório e contas)
>
> -- [artigo 173.o, n.o 1, do
> CC](https://dre.pt/web/guest/legislacao-consolidada/-/lc/123928118/201912131603/73747279/diploma/indice)


### Convocar uma Assembleia Geral Extraordinária

Outras reuniões extraordinárias podem ser convocadas por um grupo de
sócios com número igual à quinta parte do total de associados. Porém,
os estatutos podem definir um número menor que esse.

* A reunião da Assembleia-Geral deve ser marcada e anunciada com pelo
  menos 8 dias de antecedência

* Q: É mesmo preciso notificar para a assembleia geral por
  carta?
A:

* Q: Quem pode participar nas AGs?

> **Good to know:** Na Assembleia-Geral podem participar todos os
> sócios, a não ser que os estatutos definam excepções.

### Motivos de convocação
* mesa geral convoca assembleia anual (**obrigatório**)
* por algum órgão específico se definido nos estatutos q isso é
  possivel e em q circunstancias
* pelo menos 1/5 dos sócios exigem uma AG Extraordinária
* qualquer sócio pode exigir, caso ja tenha passado o prazo esperado
  para a AG anual.
* estatutos podem outros casos (não se aplica a nós)

### O que é que se vai decidir?

A ordem de trabalhos deve ser anunciada quando a convocação é
feita. Em geral na assembleia pode-se de decidir sobre qualquer
matéria. Mas é da sua comptência em particular:
1. Destituição dos titulares dos órgãos da associação;
2. Aprovação do balanço;
3. Alteração dos estatutos;
4. Extinção da associação;
5. Autorização para esta demandar os administradores por factos
praticados no exercício do cargo (cf. artigo 172.o, n.o 2).

### Podem ser online! (se definirmos nos estatutos)
> Pode justificar-se, também à semelhança do regime introduzido no
CSC, a autorização estatutária de assembleias gerais "virtuais", que
dispensem a presença física dos associados, que nelas poderão
participar através de vídeo-conferência, conferência telefónica ou de
meios telemáticos equivalentes 135


Início da AG
------------


Durante a AG | Ordem de trabalhos
---------------------------------

O que fazer no decorrer da assembleia

### Registo em Acta
As decisões tomadas na reunião têm de ficar registadas num Livro de
Actas. Este documento pode ser constituído por folhas soltas numeradas
sequencialmente e rubricadas pelos representantes do órgão a que
pertence. Cada um dos órgãos deve ter um Livro de Actas próprio e por
cada reunião deve ser elaborada uma acta. O Livro de Actas deve
respeitar um termo de abertura e tem de ser apresentado num Serviço de
Finanças a fim de ser pago o respectivo imposto de selo.

> As atas devem depois ser apresentadas num servico de finanças para
> ser pago o respetivo impost de selo ([FAQ pergunta 24][FAQ
> Associação na Hora]) ???


### Conflito de interesses em Votações
> consagra-se o impedimento de voto nas situações em que haja conflito
de interesses entre a associação e o associado, seu cônjuge,
ascendentes ou descendentes ([CC. artigo
176](https://dre.pt/web/guest/legislacao-consolidada/-/lc/123928118/201912131603/73747279/diploma/indice)). ([Fonte](www.cej.mj.pt/cej/recursos/ebooks/civil/eb_associacoes2018.pdf#page=102))




## Encerramento da AG
## Após assembleia geral

Há varios pontos da ordem de trabalhos que requerem trabalho
burocrárico extra, por exemplo, para alterar os órgãos estatuários.

### Eleição / Alteração de membros da direção

Comunicar altaração de membros da direção:

| Entidade | Prazo (desde AG) | O que fazer |
|----------|------------------|-------------|
| **Finanças** | 15 dias ([Art.º 32º nº 2 do CIVA, art.º 112 nº 2 do CIRS e art.º 118º nº 5 alínea a)](https://info.portaldasfinancas.gov.pt/pt/apoio_contribuinte/questoes_frequentes/Pages/faqs-00312.aspx#heading01)) | [alterar membros da direção no portal das finanças](ajuda-portal-financas.md#Alterar-membros-direcao)
| **Banco** |  | atualizar quem movimenta a conta
| [**Beneficiário Efetivo**](https://rcbe.justica.gov.pt/) | 30 dias ([art. 14 da Lei n.º 89/2017](https://justica.gov.pt/Guias/guia-do-registo-central-do-beneficiario-efetivo-rcbe)) | alterar membros da direção
| **Segurança Social** | 10 dias uteis ([fonte - pag 5](https://www.seg-social.pt/documents/10152/24700/2001_inscricao_alteracao_dados/5e1af577-120e-4ff9-9296-583bf230296f#page-5)) | registo de alteração dos [MOEs]((glossario.md#membros-de-orgaos-estatutarios))


----------------------------------------------------



Primeira Assembleia Geral
-------------------------

A primeira assembleia geral após a constituição da associação

>Na primeira Assembleia não poderá tomar decisões sem que metade dos
>seus membros esteja presente

### Eleição dos Elementos dos Órgãos

Nesta assembleia devem também eleger-se os membros de cada um dos
órgãos da associação. As associações são compostas por três órgãos:
Assembleia-Geral, Administração, Conselho Fiscal.

#### Assbleia Geral
A Assembleia-Geral é dirigida por uma Mesa, com três elementos eleitos
(um presidente, um vogal e um secretário), que tem como funções a
destituição dos titulares de todos os órgãos da associação, a
aprovação do plano de actividades, dos estatutos e dos balanços e a
extinção da associação.

#### Administração
Já a Administração é constituída por três pessoas (um presidente, um
secretário e um tesoureiro) e é responsável pela direcção e gestão da
associação.

#### Conselho Fiscal
O Conselho Fiscal, também com um mínimo de três sócios (um presidente,
um secretário e um redactor), faz essencialmente o controlo das contas
da associação.


## Créditos
Muitos destes textos foram retirados do [site da câmara do
porto](https://web.archive.org/web/20181013182403/https://cdp.portodigital.pt/empreendedorismo/como-criar-uma-associacao/passoas-para-a-criacao-de-uma-associacao/).


[FAQ Associação na Hora]: http://www.associacaonahora.mj.pt/seccoes/ANH-Faqs.pdf


Condução da Assembleia Geral
----------------------------

A condução das assembleias gerais é feita pelo órgão designado "mesa
da Assembleia".

Este órgão não é legamente obrigado a existir, mas é recomendado na
prática

### Presidente da Mesa da Assembleia
Compte-lhe dirigir a assembleia

### Secretários(as)





Como fazer uma boa Assembleia Geral?
------------------------------------


### Dicas

- **Fazer AG a seguir a um outro evento** em que se sabe que vários
  membros vão estar presentes. Assim, aproveita-se a oportunidade de
  alguns dos membros já estarem juntos e permite-se se apareçam uns
  quantos que não iriam à AG se fosse implicasse uma deslocação só
  para esse assunto.
