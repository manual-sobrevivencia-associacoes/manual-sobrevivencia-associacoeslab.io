# Ajuda no portal finanças

O [portal das
finanças](https://www.portaldasfinancas.gov.pt/at/html/index.html) é
particularmente conhecido por dar muitas dores de cabeça. Este
apêndice do manual pretende suavizar as várias operações.

## Alterar membros da direção
Documentos necessários: nenhum
Requisitos: [assembleia geral com eleições](assembleias-gerais.md#eleição-alteração-de-membros-da-direção)

1. Ir ao https://www.portaldasfinancas.gov.pt
2. Navegar: “todos os serviços” » “alteração de atividade” » “entrega
    de alteração de atividade” » “entregar declaração” » “entrega de
    declaração de alteração de atividade”
3. Remover membros da direção com data de término de funções
4. Adicionar novos membros
5. Validar e Submeter
6. Guardar declaração junto do restos das burocracias da associação


